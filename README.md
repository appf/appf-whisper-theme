# APPF Whisper Theme

To use this theme in your hugo project

```bash
cd themes
git submodule add https://gitlab.com/appf/appf-whisper-theme.git
```

and change your `config.yaml` (or `config.toml`) to include:

```yaml
theme: appf-whisper-theme
```

You should add the following to your gitignore in your hugo project:
```
**/exampleSite/*
resources/*
themes/*
```

example `.gitlab-ci.yml`, assuming your hugo project lives in ./site/

```yaml
# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
    - hugo --source site 
  except:
    - main

pages:
  script:
    - hugo --source site --minify --destination ../public
  artifacts:
    paths:
      - public
  only:
    - main
```

This theme is a heavy modification of [Hugo Whisper Theme](https://github.com/zerostaticthemes/hugo-whisper-theme).